# Cocos2dx Helper:

Cocos2dx Helper supply quickly solution to create Scene, Sprite.. class in Cocos2d-x (C++)

## Instroduction:

Using command:

```
cchelp <option> <name>
```

option: 
* -sce: Scene
* -spr: Sprite

name: class name separated by space symbol

## Using on your MAC:

Download CocosHelper.jar and put in your software folder.

Create script executable file (ex: cchelp):

```
#! /bin/sh
java -jar /path/to/CocosHelper.jar "$@"
```

Open terminal and type:

```
chmod u+x cchelp
```

Open /Users/username/.bash_profile and add below line:

```
export PATH="$PATH:path/to/folder/contain/cchelp"
```

Execute in terminal:

```
source /Users/username/.bash_profile
```

Enjoy!

## Using on other OS:
Updating..